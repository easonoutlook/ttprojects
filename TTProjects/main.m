//
//  main.m
//  TTProjects
//
//  Created by Eason on 9/12/13.
//  Copyright (c) 2013 www.freelync.com. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
